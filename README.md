# Practica 4 ANDROID API LARAVEL

Este repo es una extensión del repositorio https://gitlab.com/jdamiancabello/laravelweb. Aqui tenemos la aplicación android que hace llamadas a laravel mediante una API (las llamadas de la API estan detalladas en el repositorio de Laravel antes citado).

Esta aplicación hace uso de las librerías:

 - `OkHttpClient`: sirve para intercambio de datos y medios. Mas información [aquí](https://square.github.io/okhttp/).
 - `Gson`: serializa y deserializa objetos Java en JSON. Mas información [aquí](https://github.com/google/gson).
 - `Retrofit`: cliente HTTP.  Mas información [aquí](https://square.github.io/retrofit/)
