package com.example.paddleapp.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.paddleapp.R;
import com.example.paddleapp.adapter.AppointmentsAdapter;
import com.example.paddleapp.adapter.ClickListener;
import com.example.paddleapp.adapter.RecyclerTouchListener;
import com.example.paddleapp.model.Appointment;
import com.example.paddleapp.network.ApiTokenRestClient;
import com.example.paddleapp.util.SharedPreferencesManager;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListActivity extends AppCompatActivity {

    public static final int ADD_CODE = 100;
    public static final int UPDATE_CODE = 200;
    public static final int OK = 1;
    public static final String MAIL = "mail";

   @BindView(R.id.floatingActionButton) FloatingActionButton fab;

    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    int positionClicked;
    private AppointmentsAdapter adapter;

    ProgressDialog progreso;
    //ApiService apiService;
    SharedPreferencesManager preferences;

    @OnClick(R.id.floatingActionButton)
    public void Click(View view) {
        Intent i = new Intent(this, AddActivity.class);
        startActivityForResult(i, ADD_CODE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        ButterKnife.bind(this);
        preferences = new SharedPreferencesManager(this);
        //showMessage("panel: " + preferences.getToken());

        //Initialize RecyclerView
        adapter = new AppointmentsAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        //manage click
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                showPopup(view, position);
            }
            @Override
            public void onLongClick(View view, int position) {
                Intent emailIntent = new Intent(getApplicationContext(), EmailActivity.class);
                emailIntent.putExtra("appointment", ((AppointmentsAdapter)recyclerView.getAdapter()).getAt(position));
                startActivity(emailIntent);
            }
        }));

        ApiTokenRestClient.deleteInstance();

        downloadSites();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case R.id.exit:
                preferences.saveToken(null, null);
                startActivity(new Intent(getApplicationContext(), SignInActivity.class));
                finish();
                break;
        }
        return true;
    }

    private void downloadSites() {
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);
        progreso.show();
        Call<ArrayList<Appointment>> call = ApiTokenRestClient.getInstance(preferences.getToken()).getSites();
        call.enqueue(new Callback<ArrayList<Appointment>>() {
            @Override
            public void onResponse(Call<ArrayList<Appointment>> call, Response<ArrayList<Appointment>> response) {
                progreso.dismiss();
                if (response.isSuccessful()) {
                    adapter.setRepos(response.body());
                    showMessage("Datos cargados");
                } else {
                    StringBuilder message = new StringBuilder();
                    message.append("ERROR: " + response.code());
                    if (response.body() != null)
                        message.append("\n" + response.body());
                    if (response.errorBody() != null)
                        try {
                            message.append("\n" + response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    showMessage(message.toString());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Appointment>> call, Throwable t) {
                progreso.dismiss();
                if (t != null)
                    showMessage("ERROR:" + t.getMessage());
            }
        });
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Appointment appointment = new Appointment();

        if (requestCode == ADD_CODE)
            if (resultCode == OK) {
                appointment.setId(data.getIntExtra("id", 1));
                appointment.setDay(data.getStringExtra("name"));
                appointment.setTimestart(data.getStringExtra("link"));
                appointment.setTimefinish(data.getStringExtra("email"));
                adapter.add(appointment);
            }

        if (requestCode == UPDATE_CODE)
            if (resultCode == OK) {
                appointment.setId(data.getIntExtra("id", 1));
                appointment.setDay(data.getStringExtra("name"));
                appointment.setTimestart(data.getStringExtra("link"));
                appointment.setTimefinish(data.getStringExtra("email"));
                adapter.modifyAt(appointment, positionClicked);
            }
    }

    private void showPopup(View v, final int position) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.getMenuInflater().inflate(R.menu.popup_change, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.modify_site:
                        modify(adapter.getAt(position));
                        positionClicked = position;
                        return true;
                    case R.id.delete_site:
                        confirm(adapter.getAt(position).getId(), adapter.getAt(position).getDay(), position);
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.show();
    }

    private void modify(Appointment s) {
        Intent i = new Intent(this, UpdateActivity.class);
        i.putExtra("site", s);
        startActivityForResult(i, UPDATE_CODE);
    }

    private void confirm(final int idSite, String name, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to delete this appointment?")
                .setTitle("Delete")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        connection(position);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    private void connection(final int position) {
        Call<ResponseBody> call = ApiTokenRestClient.getInstance(preferences.getToken()).deleteSite(adapter.getId(position));
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);
        progreso.show();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progreso.dismiss();
                if (response.isSuccessful()) {
                    adapter.removeAt(position);
                    showMessage("Reserva borrada");
                } else {
                    StringBuilder message = new StringBuilder();
                    message.append("ERROR: " + response.code());
                    if (response.body() != null)
                        message.append("\n" + response.body());
                    if (response.errorBody() != null)
                        try {
                            message.append("\n" + response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    showMessage(message.toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progreso.dismiss();
                if (t != null)
                    showMessage("ERROR: " + t.getMessage());
            }
        });
    }
}
