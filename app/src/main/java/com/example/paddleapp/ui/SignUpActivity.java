package com.example.paddleapp.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.paddleapp.R;
import com.example.paddleapp.model.RegisterResponse;
import com.example.paddleapp.network.ApiRestClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;

    EditText nameText;
    EditText emailText;
    EditText passwordText;
    EditText reEnterPasswordText;
    Button registerButton;
    Button loginButton;

    public void register() {
        if (validate() == false) {
            showMessage("Error de registro");
            registerButton.setEnabled(true);
        } else {
            sendToServer();
        }

    }

    public void login() {
        Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
        setResult(RESULT_CANCELED, null);
        startActivity(intent);
        finish();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        setTitle("Crear cuenta");

        passwordText = findViewById(R.id.input_password);
        emailText = findViewById(R.id.input_email);
        nameText = findViewById(R.id.input_name);
        reEnterPasswordText = findViewById(R.id.input_reEnterPassword);
        registerButton = findViewById(R.id.registerButton);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });
        loginButton = findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
        }

    public boolean validate() {
        boolean valid = true;

        String name = nameText.getText().toString();
        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();
        String reEnterPassword = reEnterPasswordText.getText().toString();

        if (name.isEmpty() || name.length() < 8) {
            nameText.setError("El nombre debe tener 8 caracteres al menos");
            valid = false;
        } else {
            nameText.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailText.setError("Email no válido");
            valid = false;
        } else {
            emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            passwordText.setError("La contraseña debe ser entre 4 y 10 caracteres");
            valid = false;
        } else {
            passwordText.setError(null);
        }

        if ( !(reEnterPassword.equals(password))) {
            reEnterPasswordText.setError("Las contraseñas no coinciden");
            valid = false;
        } else {
            reEnterPasswordText.setError(null);
        }

        return valid;
    }

    private void sendToServer() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Subiendo datos...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        registerButton.setEnabled(false);

        final String name = nameText.getText().toString();
        final String email = emailText.getText().toString();
        final String password = passwordText.getText().toString();

        Call<RegisterResponse> call = ApiRestClient.getInstance().register(name, email, password);
        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    Log.d("onResponse", "" + response.body().getToken());
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("email", email );
                    resultIntent.putExtra("password", password );
                    resultIntent.putExtra("token", response.body().getToken() );
                    setResult(RESULT_OK, resultIntent);
                    finish();
                } else {
                    try {
                        JSONObject errorObject = new JSONObject(response.errorBody().string());
                        showMessage(errorObject.getString("error"));
                    } catch (IOException e) {
                        e.printStackTrace();
                        showMessage(e.getMessage());
                    } catch (JSONException e) {
                        e.printStackTrace();
                        showMessage(e.getMessage());
                    }
                    registerButton.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                progressDialog.dismiss();
                showMessage(t.getMessage());
                registerButton.setEnabled(true);
            }
        });
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

}
