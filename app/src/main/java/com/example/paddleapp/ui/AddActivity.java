package com.example.paddleapp.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.paddleapp.R;
import com.example.paddleapp.model.Appointment;
import com.example.paddleapp.network.ApiTokenRestClient;
import com.example.paddleapp.util.SharedPreferencesManager;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddActivity extends AppCompatActivity implements Callback<Appointment> {
    public static final int OK = 1;

    @BindView(R.id.nameSite) EditText nameSite;
    @BindView(R.id.linkSite) EditText linkSite;
    @BindView(R.id.emailSite) EditText emailSite;
    @BindView(R.id.accept) Button accept;
    @BindView(R.id.cancel) Button cancel;

    ProgressDialog progreso;
    SharedPreferencesManager preferences;
    private int y;
    private int m;
    private int d;

    @OnClick(R.id.accept)
    public void clickAccept(View view){
        String n, l, e;
        Appointment s;

        hideSoftKeyboard();
        n = nameSite.getText().toString();
        l = linkSite.getText().toString();
        e = emailSite.getText().toString();
        if (l.isEmpty() || e.isEmpty()) {
            Toast.makeText(this, "Please, fill the fields", Toast.LENGTH_SHORT).show();
            return;
        }

        // Validar otros datos aqui

        String day = String.format("%04d", y) + "-" + String.format("%02d", m)+ "-" + String.format("%02d", d);
        Toast.makeText(this, day, Toast.LENGTH_SHORT).show();

        int timestart, timefinish;
        timefinish = Integer.parseInt(e);
        timestart= Integer.parseInt(l);

        if(timestart < 9 || timestart > 22) {
            Toast.makeText(this, "Only numbers from 9AM to 10PM (22:00) allowed in the time start field", Toast.LENGTH_SHORT).show();
        }

        if(timefinish < 9 || timefinish > 22) {
            Toast.makeText(this, "Only numbers from 9AM to 10PM (22:59) allowed in the time finish field", Toast.LENGTH_SHORT).show();
        }

        Appointment appo = new Appointment(day, Integer.toString(timestart), Integer.toString(timefinish));
        connection(appo);
    }

    @OnClick(R.id.cancel)
    public void clickCancel(View view){
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        // YYYY-mm-dd
        DatePicker datePicker = (DatePicker) findViewById(R.id.dateadd);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        y = calendar.get(Calendar.YEAR);
        m = calendar.get(Calendar.MONTH) + 1;
        d = calendar.get(Calendar.DAY_OF_MONTH);
        datePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {
                y = year;
                m = month + 1;
                d = dayOfMonth;
            }
        });

        ButterKnife.bind(this);
        preferences = new SharedPreferencesManager(this);
    }

    private void connection(Appointment s) {
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);
        progreso.show();

        //Call<Appointment> call = ApiRestClient.getInstance().createSite("Bearer " + preferences.getToken(), s);
        Call<Appointment> call = ApiTokenRestClient.getInstance(preferences.getToken()).createSite(s);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<Appointment> call, Response<Appointment> response) {
        progreso.dismiss();
        if (response.isSuccessful()) {
            Appointment appointment = response.body();
            Intent i = new Intent();
            Bundle bundle = new Bundle();
            bundle.putInt("id", appointment.getId());
            bundle.putString("name", appointment.getDay());
            bundle.putString("link", appointment.getTimestart());
            bundle.putString("email", appointment.getTimefinish());
            i.putExtras(bundle);
            setResult(OK, i);
            finish();
            showMessage("Appointment added");
        } else {
            StringBuilder message = new StringBuilder();
            message.append("Download error: " + response.code());
            if (response.body() != null)
                message.append("\n" + response.body());
            if (response.errorBody() != null)
                try {
                    message.append("\n" + response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            showMessage(message.toString());
        }
    }

    @Override
    public void onFailure(Call<Appointment> call, Throwable t) {
        progreso.dismiss();
        if (t != null)
            showMessage("Failure in the communication\n" + t.getMessage());
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}

