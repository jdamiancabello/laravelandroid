package com.example.paddleapp.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.paddleapp.R;
import com.example.paddleapp.model.Appointment;
import com.example.paddleapp.network.ApiTokenRestClient;
import com.example.paddleapp.util.SharedPreferencesManager;

import java.io.IOException;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateActivity extends AppCompatActivity implements View.OnClickListener, Callback<Appointment> {
    public static final int OK = 1;

    @BindView(R.id.nameSite) EditText nameSite;
    @BindView(R.id.linkSite) EditText linkSite;
    @BindView(R.id.emailSite) EditText emailSite;
    @BindView(R.id.accept) Button accept;
    @BindView(R.id.cancel) Button cancel;

    ProgressDialog progreso;
    Appointment s;
    SharedPreferencesManager preferences;
    private int y;
    private int m;
    private int d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        ButterKnife.bind(this);
        preferences = new SharedPreferencesManager(this);

        accept.setOnClickListener(this);
        cancel.setOnClickListener(this);

        Intent i = getIntent();
        s = (Appointment) i.getSerializableExtra("site");
        //int day = Integer.parseInt();

        // YYYY-mm-dd

        y = Integer.parseInt(s.getDay().substring(0, 4));
        m = Integer.parseInt(s.getDay().substring(5, 7)) - 1;
        d = Integer.parseInt(s.getDay().substring(8, 10));

        DatePicker datePicker = (DatePicker) findViewById(R.id.dateedit);
        datePicker.init(y, m, d, new DatePicker.OnDateChangedListener() {

            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {
                y = year;
                m = month + 1;
                d = dayOfMonth;
            }
        });

        linkSite.setText(s.getTimestart());
        emailSite.setText(s.getTimefinish());
    }

    @Override
    public void onClick(View v) {
        String n, l, e;

        hideSoftKeyboard();
        if (v == accept) {
            l = linkSite.getText().toString();
            e = emailSite.getText().toString();
            if (e.isEmpty() || l.isEmpty())
                Toast.makeText(this, "Please, fill the name and the link", Toast.LENGTH_SHORT).show();
            else {
                s.setDay(String.format("%04d", y) + "-" + String.format("%02d", m)+ "-" + String.format("%02d", d));
                s.setTimestart(l);
                s.setTimefinish(e);
                connection(s);
            }
        }
        if (v == cancel)
            finish();
    }

    private void connection(Appointment s) {
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);
        progreso.show();

        //Call<Appointment> call = ApiRestClient.getInstance().updateSite("Bearer " + preferences.getToken(), s, s.getId());
        Call<Appointment> call = ApiTokenRestClient.getInstance(preferences.getToken()).updateSite(s, s.getId());
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<Appointment> call, Response<Appointment> response) {
        progreso.dismiss();
        if (response.isSuccessful()) {
            Appointment appointment = response.body();
            Intent i = new Intent();
            Bundle bundle = new Bundle();
            bundle.putInt("id", appointment.getId());
            bundle.putString("name", appointment.getDay());
            bundle.putString("link", appointment.getTimestart());
            bundle.putString("email", appointment.getTimefinish());
            i.putExtras(bundle);
            setResult(OK, i);
            finish();
            showMessage("Modified appointment ok");
        } else {
            StringBuilder message = new StringBuilder();
            message.append("Download error: " + response.code());
            if (response.body() != null)
                message.append("\n" + response.body());
            if (response.errorBody() != null)
                try {
                    message.append("\n" + response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            showMessage(message.toString());
        }
    }

    @Override
    public void onFailure(Call<Appointment> call, Throwable t) {
        progreso.dismiss();
        if (t != null)
            showMessage("Failure in the communication\n" + t.getMessage());
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}