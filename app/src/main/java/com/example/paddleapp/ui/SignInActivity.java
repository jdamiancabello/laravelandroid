package com.example.paddleapp.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.paddleapp.R;
import com.example.paddleapp.model.LoginResponse;
import com.example.paddleapp.network.ApiRestClient;
import com.example.paddleapp.util.SharedPreferencesManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends AppCompatActivity {

    public static final String APP = "Reservas";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String TOKEN = "token";
    private static final int REQUEST_REGISTER = 1;

    SharedPreferencesManager preferences;
    EditText emailText;
    EditText passwordText;
    Button loginButton;
    Button registerButton;
    private ProgressDialog progressDialog;

    public void login() {
        if (!validate())
            showMessage("Error al validar los datos");
        else
            loginByServer();
    }

    public void register() {
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivityForResult(intent, REQUEST_REGISTER);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        setTitle("Entrar");

        emailText = findViewById(R.id.input_email);
        passwordText = findViewById(R.id.input_password);
        loginButton = findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
        registerButton = findViewById(R.id.registerButton);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

        preferences = new SharedPreferencesManager(this);
        emailText.setText(preferences.getEmail());
        passwordText.setText(preferences.getPassword());
    }

    private void loginByServer() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Cargando...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        loginButton.setEnabled(false);

        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        Call<LoginResponse> call = ApiRestClient.getInstance().login(email, password);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                progressDialog.dismiss();
                //onRegisterSuccess();
                if (response.isSuccessful()) {
                    Log.d("onResponse", "" + response.body());
                    preferences.save(emailText.getText().toString(), passwordText.getText().toString(), response.body().getToken());
                    startActivity(new Intent(getApplicationContext(), ListActivity.class));
                    finish();
                } else {
                    try {
                        JSONObject errorObject = new JSONObject(response.errorBody().string());
                        showMessage(errorObject.getString("error"));
                    } catch (IOException e) {
                        e.printStackTrace();
                        showMessage(e.getMessage());
                    } catch (JSONException e) {
                        e.printStackTrace();
                        showMessage(e.getMessage());
                    }
                    loginButton.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressDialog.dismiss();
                showMessage(t.getMessage());
                loginButton.setEnabled(true);
            }
        });
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_REGISTER) {
            if (resultCode == RESULT_OK) {
                preferences.save(
                        data.getExtras().getString("email"),
                        data.getExtras().getString("password"),
                        data.getExtras().getString("token"));
                startActivity(new Intent(this, ListActivity.class));
                finish();
            }
        }
    }

    public boolean validate() {
        boolean valid = true;

        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailText.setError("No es un email válido");
            requestFocus(emailText);
            valid = false;
        } else {
            emailText.setError(null);
        }

        if (password.isEmpty()) {
            passwordText.setError("Introduzca su contraseña");
            requestFocus(passwordText);
            valid = false;
        } else {
            passwordText.setError(null);
        }

        return valid;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}

