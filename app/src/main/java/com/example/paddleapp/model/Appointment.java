package com.example.paddleapp.model;

import java.io.Serializable;

public class Appointment implements Serializable {
    private int id;
    private String day;
    private String timestart;
    private String timefinish;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getDay() {
        return day;
    }
    public void setDay(String day) {
        this.day = day;
    }
    public String getTimestart() {
        return timestart;
    }
    public void setTimestart(String timestart) {
        this.timestart = timestart;
    }
    public String getTimefinish() {
        return timefinish;
    }
    public void setTimefinish(String timefinish) {
        this.timefinish = timefinish;
    }
    public Appointment(int id, String day, String timestart, String timefinish) {
        super();
        this.id = id;
        this.day = day;
        this.timestart = timestart;
        this.timefinish = timefinish;
    }
    public Appointment(String day, String timestart, String timefinish) {
        super();
        this.day = day;
        this.timestart = timestart;
        this.timefinish = timefinish;
    }

    public Appointment() {}

    @Override
    public String toString() {
        return  "the " + day + " at " + String.format("%02d", Integer.parseInt(timestart)) + ":00";
    }
}
