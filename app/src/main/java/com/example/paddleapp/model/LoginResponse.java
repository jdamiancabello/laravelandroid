package com.example.paddleapp.model;

public class LoginResponse {
    private String token;
    //private String error;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
