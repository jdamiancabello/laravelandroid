package com.example.paddleapp.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.paddleapp.ui.SignInActivity;

public class SharedPreferencesManager {

    //public static final String APP = "MyApp";
    //public static final String EMAIL = "email";
    //public static final String PASSWORD = "password";
    //public static final String TOKEN = "token";

    SharedPreferences sp;
    SharedPreferences.Editor spEditor;

    public SharedPreferencesManager(Context context){
        sp = context.getSharedPreferences(SignInActivity.APP, Context.MODE_PRIVATE);
        spEditor = sp.edit();
    }

    public void save (String email, String password, String token){
        spEditor.putString(SignInActivity.EMAIL, email);
        spEditor.putString(SignInActivity.PASSWORD, password);
        spEditor.putString(SignInActivity.TOKEN, token);
        spEditor.apply();
    }
    public void saveEmail(String key, String value){
        spEditor.putString(key, value);
        spEditor.apply();
    }

    public String getEmail(){
        return sp.getString(SignInActivity.EMAIL, "");
    }

    public void savePassword(String key, String value){
        spEditor.putString(key, value);
        spEditor.apply();
    }

    public String getPassword(){
        return sp.getString(SignInActivity.PASSWORD, "");
    }

    public void saveToken(String key, String value){
        spEditor.putString(key, value);
        spEditor.apply();
    }

    public String getToken(){
        return sp.getString(SignInActivity.TOKEN, "");
    }
}
