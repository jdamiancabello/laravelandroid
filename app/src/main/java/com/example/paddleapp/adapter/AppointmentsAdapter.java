package com.example.paddleapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.paddleapp.R;
import com.example.paddleapp.model.Appointment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by paco on 6/02/18.
 */

public class AppointmentsAdapter extends RecyclerView.Adapter<AppointmentsAdapter.ViewHolder> {
    private ArrayList<Appointment> appointments;

    public AppointmentsAdapter(){
        this.appointments = new ArrayList<>();
    }

    public void setRepos(ArrayList<Appointment> repos) {
        appointments = repos;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.textView1) TextView day;
        @BindView(R.id.textView2) TextView timestart;
        @BindView(R.id.textView3) TextView timefinish;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();

        // Inflate the custom layout
        LayoutInflater inflater = LayoutInflater.from(context);
        View siteView = inflater.inflate(R.layout.item_view, parent, false);

        // Return a new holder instance
        return new ViewHolder(siteView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Appointment appointment = appointments.get(position);

        holder.day.setText(appointment.getDay());
        holder.timestart.setText(appointment.getTimestart());
        holder.timefinish.setText(appointment.getTimefinish());
    }

    @Override
    public int getItemCount() {
        return appointments.size();
    }

    public int getId(int position){

        return this.appointments.get(position).getId();
    }

    public Appointment getAt(int position){
        Appointment appointment;
        appointment = this.appointments.get(position);
        return appointment;
    }

    public void add(Appointment appointment) {
        this.appointments.add(appointment);
        notifyItemInserted(appointments.size() - 1);
        notifyItemRangeChanged(0, appointments.size() - 1);
    }

    public void modifyAt(Appointment appointment, int position) {
        this.appointments.set(position, appointment);
        notifyItemChanged(position);
    }

    public void removeAt(int position) {
        this.appointments.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(0, appointments.size() - 1);
    }
}
