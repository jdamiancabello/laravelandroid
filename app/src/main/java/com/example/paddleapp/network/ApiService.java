package com.example.paddleapp.network;

import com.example.paddleapp.model.Appointment;
import com.example.paddleapp.model.Email;
import com.example.paddleapp.model.LoginResponse;
import com.example.paddleapp.model.LogoutResponse;
import com.example.paddleapp.model.RegisterResponse;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiService {
    @FormUrlEncoded
    @POST("api/register")
    Call<RegisterResponse> register(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password);

    //@POST("api/register")
    //Call<RegisterResponse>register(@Body User user);

    @FormUrlEncoded
    @POST("api/login")
    Call<LoginResponse> login(
            @Field("email") String email,
            @Field("password") String password);
    //@POST("api/login")
    //Call<LoginResponse>login(@Body User user);

    @POST("api/logout")
    Call<LogoutResponse> logout(
            @Header("Authorization") String token);

    @GET("api/sites")
    Call<ArrayList<Appointment>> getSites(
            @Header("Authorization") String token);

    //@GET("acceso/sites.json")
    //Call<ArrayList<Appointment>> getLocalSites();

    @POST("api/appointments")
    Call<Appointment> createSite(
            @Header("Authorization") String token,
            @Body Appointment appointment);

    @PUT("api/appointments/{id}")
    Call<Appointment> updateSite(
            @Header("Authorization") String token,
            @Body Appointment appointment,
            @Path("id") int id);

    @DELETE("api/appointments/{id}")
    Call<ResponseBody> deleteSite(
            @Header("Authorization") String token,
            @Path("id") int id);

    @POST("api/email")
    Call<ResponseBody> sendEmail(@Body Email email);
}

